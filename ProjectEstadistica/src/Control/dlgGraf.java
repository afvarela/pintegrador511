package Control;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import Control.claseDatos;
import Control.MethodsFrencuencia;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public class dlgGraf extends JDialog {

	private static final long serialVersionUID = 1L;
	private MethodsFrencuencia frecuencia;

    public dlgGraf(MethodsFrencuencia frec, Frame owner, boolean modal) {
        super(owner, modal);
        frecuencia = frec;

        JTabbedPane tbnPane = new JTabbedPane();

        JPanel contenedor = new JPanel(new FlowLayout());
        JPanel botonera = new JPanel(new FlowLayout());
        JButton btnCerrar = new JButton("Cerrar");
        btnCerrar.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        botonera.add(btnCerrar);

        tbnPane.add("Histograma", panelHistograma());
        tbnPane.add("Circular", panelCircular());
        tbnPane.add("Ojiva", panelPoligono());
        


        setLayout(new BorderLayout(5, 5));
        add(tbnPane, BorderLayout.CENTER);
        add(botonera, BorderLayout.SOUTH);
        pack();
    }

    private JPanel panelHistograma() {
        JFreeChart jfreechart = crearHistograma(crearDatosHistograma());
        ChartPanel panel = new ChartPanel(jfreechart);
        return panel;
    }
     private JPanel panelCircular() {
        JFreeChart jfreechart = crearCircular(crearDatosCircular());
        ChartPanel panel = new ChartPanel(jfreechart);
        return panel;
    }
   

    private JFreeChart crearHistograma(IntervalXYDataset datos) {
        JFreeChart jfreechart = ChartFactory.createXYBarChart("Grafico Histograma", "X", false, "Y", datos, PlotOrientation.VERTICAL, false, false, false);
        return jfreechart;
    }
    private JFreeChart crearCircular(DefaultPieDataset datos) {
        JFreeChart jfreechart = ChartFactory.createPieChart("Grafico Circular", crearDatosCircular());
        return jfreechart;
    }
     private JPanel panelPoligono() {
        JFreeChart jfreechart = crearPoligono(crearDatosPoligono());
        ChartPanel chart = new ChartPanel(jfreechart);
        return chart;
    }
      private JFreeChart crearPoligono(XYDataset datosPoligono) {
        JFreeChart jfreechart = ChartFactory.createXYLineChart("Grafico Ojiva", "X", "Y", datosPoligono, PlotOrientation.VERTICAL, false, false, false);
        return jfreechart;
    }
      
      private XYDataset crearDatosPoligono() {
        XYSeries poligono = new XYSeries("Poligono");

        claseDatos[] clase = frecuencia.getClases();
        float[] rel = frecuencia.getFrecRel();
        int k = frecuencia.getK();
        for (int i = 0; i < clase.length; i++) {
            poligono.add(clase[i].getMarca(), rel[i]);
        }
        XYSeriesCollection series = new XYSeriesCollection();
        series.addSeries(poligono);
        return series;
    }
    

    private IntervalXYDataset crearDatosHistograma() {
        SimpleIntervalXYDataset dataset = new SimpleIntervalXYDataset(frecuencia);
        return dataset;
    }
     private float redondear(float d, int i) {
        float valor = 0;
        int factor=10*(i+1);
        valor = d;
        valor = valor * factor;
        valor = java.lang.Math.round(valor);
        valor = valor / factor;

        return valor;
    }
     private DefaultPieDataset crearDatosCircular() {
         DefaultPieDataset  dataset = new DefaultPieDataset();
         claseDatos[] clases = frecuencia.getClases();
         int n = frecuencia.getK();
         float[] rel = frecuencia.getFrecRel();
         for (int i = 0; i < n; i++) {
         String str = "[ " + Math.round(clases[i].getLimInf()) + "," + Math.round(clases[i].getLimSup()) + " >";
        // int valor = (int) redondear(rel[i], 4);
         dataset.setValue(str,redondear(rel[i], 4));
         }
     
         return dataset;
    }
  

  

   
}
