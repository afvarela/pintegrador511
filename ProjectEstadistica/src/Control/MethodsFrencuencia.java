
package Control;

import static com.sun.javafx.tk.Toolkit.getToolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;


public class MethodsFrencuencia {
    

    // Atributos

    private float[] valor;
 

    public MethodsFrencuencia() {
        valor = new float[10];
    }

    public MethodsFrencuencia(float[] valores) {
        valor = valores;
    }

    public void setValor(float[] valor) {
        this.valor = valor;
    }

    public float[] getValor() {
        return valor;
    }

    /**
     * Retorna el maximo valor de la serie
     * @return
     */
    public float getMax() {
        float max = valor[0];
        for (int i = 0; i < valor.length; i++) {
            if (max < valor[i]) {
                max = valor[i];
            }
        }
        return max;
    }
    /*calcula la media*/
    public float getMedia(){
        float media = 0;
        for (int i = 0; i < valor.length; i++) {
             media = media + valor[i];
        }
        return Math.round(media / valor.length);
    }
    /*calcular la moda*/
     public float getModa(){
       float Repite = 0;
       float moda = 0;
        for (int i = 0; i < valor.length; i++) {
            int CantRepite = 0;
            for (int j = 0; j < valor.length; j++) {
                if (valor[i] == valor[j]) {
                    CantRepite++;
                }
            }
            if (CantRepite > Repite) {
                moda = valor[i];
                Repite = CantRepite;
            }
        }
        return moda;
    }

    /**
     * Retorna el minimo valor de la serie
     * @return
     */
    public float getMin() {
        float min = valor[0];
        for (int i = 0; i < valor.length; i++) {
            if (min > valor[i]) {
                min = valor[i];
            }
        }
        return min;
    }

    /**
     * Retorna el rango
     */
    public float getRango() {
        return getMax() - getMin();
    }

    /**
     * Retorna el numero de intervalos
     */
    public int getK() {
       // return Math.round(Math.round((1 + 3.3 * Math.log10(valor.length))+0.5));
       //return (int) Math.round(Math.sqrt(valor.length));
           return (int) ((int) Math.round(1+3.3 * Math.log10(valor.length)));
    }

    /**
     * Retorna la Amplitud de Clase
     */
    public int getA() {
        return Math.round(getRango() / getK()) ;
        // return (int) (getRango() / getK());
    }

    /**
     * Devuelve el limite Real inferior
     */
    public float getLimiteInferior() {
        return (getMin() - 0.5f);
      
    }

    /**
     * Devuelve el limite Real superior
     */
    public float getLimiteSuperior() {
        return getMax() + 0.5f;
    }
    
   

    public claseDatos[] getClases() {
        //claseDatos[] limites = new claseDatos[getK()];
        claseDatos[] limites = new claseDatos[getK()];
        limites[0] = new claseDatos(getMin(), getMin() + getA());
        
        for (int i = 1; i < limites.length; i++) {
            claseDatos anterior = limites[i - 1];
            //limites[i] = new claseDatos(anterior.getLimSup(), anterior.getLimSup() + getA() );
            limites[i] = new claseDatos(anterior.getLimSup(), anterior.getLimSup() + getA() );
         
        }
        return limites;
    }

    /**
     * Retorna la frecuencia relativa de la serie de datos
     */
    public int[] getFrecAbs() {
        int[] frec = new int[getK()];
        claseDatos clase[] = getClases();
        for (int i = 0; i < frec.length-1; i++) {
            frec[i] = contar(clase[i].getLimInf(), clase[i].getLimSup());
        }
        int ultimo=frec.length-1;
        frec[frec.length-1]=contar(clase[ultimo].getLimInf(), clase[ultimo].getLimSup()+0.5f);
        return frec;
    }
    
    public void Numeros(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                //if(Character.isLetter(c)){
                if (e.getKeyChar() > 32 && e.getKeyChar() < 47 || e.getKeyChar() > 58) {
                    getToolkit();
                    e.consume();
                }
            }

        });
    }
    
    

    /**
     * Retorna las frecuencias absolutas acumuladas
     */
    public int[] getFrecAbsAc() {
        int[] ac = new int[getK()];
        int[] abs = getFrecAbs();
        ac[0] = abs[0];
        for (int i = 1; i < ac.length; i++) {
            ac[i] = ac[i - 1] + abs[i];
        }
        return ac;
    }

    /**
     * Retorna las frecuencias relativas de la serie
     */
    public float [] getFrecRel() {
        float[] rel = new float[getK()];
        int[] abs = getFrecAbs();
        for (int i = 0; i < rel.length; i++) {
            rel[i] = Float.parseFloat(abs[i]+"")  / Float.parseFloat(valor.length+"");
        }
        return rel;
    }

    /**
     * Retorna las frecuencias relativas acumuladas
     * @return
     */
    public float [] getFrecRelAc() {
        float[] ac = new float[getK()];
        float[] rel = getFrecRel();
        ac[0] = rel[0];
        for (int i = 1; i < ac.length; i++) {
            ac[i] = ac[i - 1] + rel[i];
        }
        return ac;
    }

    /**
     * Cuenta los numeros que estan dentro de un rango de datos
     * @param limInf
     * @param limSup
     * @return
     */
        private int contar(float limInf, float limSup) {
        int count = 0;
        for (int i = 0; i < valor.length; i++) {
            if (valor[i] >= limInf && valor[i] < limSup) {
                count++;
                
            }
         
        }
        return count;
    }
}
