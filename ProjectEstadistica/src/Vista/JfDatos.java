
package Vista;
import Control.MethodsFrencuencia;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;


public class JfDatos extends javax.swing.JInternalFrame {

   private MethodsFrencuencia frec;
    public JfDatos() {
        initComponents();
        MethodsFrencuencia objNum = new MethodsFrencuencia();
        objNum.Numeros(txt_datos);
        frec = new MethodsFrencuencia();
        jtable_datos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jtable_datos.setSelectionBackground(java.awt.Color.yellow);
        jtable_datos.setSelectionForeground(java.awt.Color.black);
    }
   
    
    public void Datos() {
        String acumulador = "";

        String values = "";
        DefaultTableModel Delete = (DefaultTableModel) jtable_datos.getModel();
        for (int i = 0; i < jtable_datos.getRowCount(); i++) {           
            values = jtable_datos.getValueAt(i, 0).toString();
           
            acumulador+= values+",";
           
        }
        
        Main.txtIngreso.setText(acumulador);
        Main.btn_calcular.setEnabled(true);
        dispose();
    }
    
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_datos = new javax.swing.JTextField();
        btn_agregar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtable_datos = new javax.swing.JTable();
        btn_eliminar = new javax.swing.JButton();
        btn_Datos = new javax.swing.JButton();

        jButton1.setText("jButton1");

        setClosable(true);
        setIconifiable(true);

        jLabel1.setText("Digite los datos:");

        txt_datos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_datosKeyPressed(evt);
            }
        });

        btn_agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        btn_agregar.setText("Agregar");
        btn_agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_agregarActionPerformed(evt);
            }
        });

        jtable_datos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Datos"
            }
        ));
        jScrollPane1.setViewportView(jtable_datos);

        btn_eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Actions-edit-delete-icon.png"))); // NOI18N
        btn_eliminar.setText("Eliminar Registro");
        btn_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarActionPerformed(evt);
            }
        });

        btn_Datos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Ok-icon.png"))); // NOI18N
        btn_Datos.setText("Enviar Datos");
        btn_Datos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_DatosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txt_datos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_agregar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_eliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_Datos))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_datos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_agregar)
                    .addComponent(btn_eliminar)
                    .addComponent(btn_Datos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_agregarActionPerformed
      if(txt_datos.getText().isEmpty()){
          JOptionPane.showMessageDialog(null,"El campo no puede estar vacio");
      }else{
      DefaultTableModel modelo = (DefaultTableModel) jtable_datos.getModel();
      int datostbl = Integer.parseInt(txt_datos.getText());
      String datos[] = {datostbl+""};
      modelo.addRow(datos);
      txt_datos.setText("");
      }
    }//GEN-LAST:event_btn_agregarActionPerformed

    private void txt_datosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_datosKeyPressed
      char teclaEnter = evt.getKeyChar();
       if(teclaEnter==KeyEvent.VK_ENTER){
           btn_agregar.doClick();
       }
    }//GEN-LAST:event_txt_datosKeyPressed

    private void btn_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarActionPerformed
       int filaSelected, op;
        try {
            filaSelected = jtable_datos.getSelectedRow();

            if (filaSelected == -1) {
                JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna linea para eliminar");
            } else {
                op = JOptionPane.showConfirmDialog(null, "¿Esta seguro de eliminar la linea?");
                if (op == JOptionPane.YES_OPTION) {

                    DefaultTableModel modeloDelete = (DefaultTableModel) jtable_datos.getModel();

                    modeloDelete.removeRow(filaSelected);

                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);

        }
    }//GEN-LAST:event_btn_eliminarActionPerformed

    private void btn_DatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_DatosActionPerformed
       if(jtable_datos.getRowCount() == 0){
        JOptionPane.showMessageDialog(null, "La tabla no puede estar vacia");
       }else{
           Datos();
       }
    }//GEN-LAST:event_btn_DatosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Datos;
    private javax.swing.JButton btn_agregar;
    private javax.swing.JButton btn_eliminar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtable_datos;
    private javax.swing.JTextField txt_datos;
    // End of variables declaration//GEN-END:variables
}
