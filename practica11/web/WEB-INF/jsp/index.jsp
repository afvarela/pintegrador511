<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>inicio 2</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/olimpicos.css">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<style type="text/css">
		body, html{
			font-family: 'PT Sans', sans-serif;
		}
	</style>
    </head>
<body>
<div class="container">

<div class="row">
<div class="col-sm-6" style="padding: 10px">
<h2><img src="img/logo-rio2016.jpg" width="200"> PI Juegos Olímpicos</h2>
</div>
<div class="col-sm-6 text-right">
<br>
	<a href=""><span class="glyphicon glyphicon-user"></span> Bienvenido <b>Product Owner</b> <span class="caret"></span></a>
	<br><br>
</div>
</div>
      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PI 511 2016</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            <li><a href="">Información</a></li>
              <li class="active"><a href="#">Deportes</a></li>
              <li><a href="#">Países</a></li><li><a href="">Cronograma</a></li>
              <li><a href="#">Escenarios</a></li>
              <li><a href="">Atletas</a></li>
              <li><a href="">Medallas</a></li>
               <li><a  href="IrRecord.html">record</a></li>
                <li><a  href="IrRecord.html">medallas</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="">Administrador</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

<!-- ENCABEZADO CIERRA -->

      <div class="row text-center text-success">
        <div class="col-sm-3">
        <h3 class="text-success">Deportes</h3>
        <img src="img/deportes.png" width="200"><br><br>
        	<p>Más de 40 deportes con sus respectivas disciplinas.</p>
        </div>
        <div class="col-sm-3"><h3 class="text-success">Atletas</h3><img src="img/deportistas.png" width="200"><br><br><p>Más de 1000 atletas y diferentes equipos.</p></div>
        <div class="col-sm-3"><h3>Delegaciones</h3><img src="img/paises.png" width="200"><br><br>
<p>Más de 150 países y delegaciones de todo el mundo.</p>
        </div>
        <div class="col-sm-3"><h3 class="text-success">Medallas</h3><img src="img/medallas.png" width="200"><br><br><p>Más de 500 medallas y diplomas olímpicos.</p></div>
      </div>
<br><br>
    </div> <!-- /container -->
    <div class="container-fluid text-center" style="background:url(img/fondo-1.jpg);">
    	<h1 style="margin: 150px auto 150px auto; padding: 15px; background:rgba(0,0,0,0.5); display: inline-block; color: white" class="text-center">Proyecto Integrador 511<br>Juegos Olímpicos</h1>
    </div>

<div class="container text-center"><br><br><br>
	<h2 class="text-success">La mejor aplicación web de todos los juegos olímpicos de la historia</h2>	
	<br><br><br><br>
</div>

<!-- PIE DE PAGINA -->
<footer class="container-fluid" style="background: #eee; padding-top: 50px; box-shadow: 0 0 20px #aaa;">
	<div class="container">
		<div class="row">
				<div class="col-sm-4 text-center">
				<a href="">Enlace número 1</a><br>
				<a href="">Enlace número 2</a><br>
				<a href="">Enlace número 3</a>
				</div>

				<div class="col-sm-4 text-center">
				<a href="">Enlace número 1</a><br>
				<a href="">Enlace número 2</a><br>
				<a href="">Enlace número 3</a>
				</div>

				<div class="col-sm-4 text-center">
				<a href="">Enlace número 1</a><br>
				<a href="">Enlace número 2</a><br>
				<a href="">Enlace número 3</a>
				</div>

				
<br><br><br><br>

		</div>
		<br><br>
		<p class="text-center"><b>&copy; 2016 Proyecto Integador 511. Todos los derechos reservados.</b><br>
		Prohibida la reproducción total o parcial.</p>
		<br><br>
		<img src="img/fondo-rio-2016-pie.png" width="100%">
	</div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>

</body> 
       
</html>
