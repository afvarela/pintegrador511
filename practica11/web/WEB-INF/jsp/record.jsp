<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>inicio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/olimpicos.css">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<style type="text/css">
		body, html{
			font-family: 'PT Sans', sans-serif;
		}
	</style>
    </head>
<body>
<div class="container">

<div class="row">
<div class="col-sm-6" style="padding: 10px">
<h2><img src="img/logo-rio2016.jpg" width="200"> PI Juegos Olímpicos</h2>
</div>
<div class="col-sm-6 text-right">
<br>
	<a href=""><span class="glyphicon glyphicon-user"></span> Bienvenido <b>Product Owner</b> <span class="caret"></span></a>
	<br><br>
</div>
</div>
      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PI 511 2016</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            <li><a href="">Información</a></li>
              <li><a href="#">Deportes</a></li>
              <li><a href="#">Países</a></li><li><a href="">Cronograma</a></li>
              <li><a href="#">Escenarios</a></li>
              <li><a href="">Atletas</a></li>
              <li><a href="">Medallas</a></li>
               <li  class="active"><a href="">Record</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="">Administrador</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      
<br>

<!-- ENCABEZADO CIERRA -->

<div class="row">

<div class="col-sm-9">

<ol class="breadcrumb">
  <li><a href="#">Inicio</a></li>
  
  <li class="active">Record</li>
</ol>

    <h1 class="text-success"><img src="img/wr.png" width="60"> Record<br><small>OLIMPICOS</small></h1>
<br>
    <img src="img/record1.jpg" width="100%">
<br><br>
<p><h3>El Record Olimpico</h3> es el record que se establece solamente dentro de las Olimpiadas y solo puede romperse dentro de las Olimpiadas</p>
<p><h3>El Record Mundial</h3> es el record que se establece a nivel mundial, puede ser conseguido en cualquier pais y en cualquier momento, dentro de cualquier competencia, pero tambien puede conseguirse dentro de las Olimpiadas al igual que puede romperse dentro y fuera de éstas.</p>
<p>
*  Cuando se rompe un record mundial dentro de las Olimpiadas tambien se rompe el record olimpico, si se rompe fuera de éstas solo se rompe el Record Mundial
<br>*  Cuando se rompe un Record Olimpico puede que no se rompa el Record Mundial porque éste ultimo es mejor
<bR>*  El Record Mundial siempre será igual o mayor que el Record Olímpico
<bR>*  El Record Olimpico siempre será igual o menor que el Record Mundial    
    
    
</p>



<p> 
En los Juegos Olímpicos de Río 2016 se superaron varios récords tanto olímpicos como mundiales. Ocurrió en atletismo, natación, tiro, pentatlón moderno, canotaje de velocidad, levantamiento de pesas, ciclismo de pista, tiro con arco y remo. Además, anunciaron su retiro Usain Bolt y Michael Phelps, quienes volvieron a dejar sus marcas.
</p>
<p>
Phelps batió un récord que duró más de 2000 años en estos juegos al lograr su decimotercer título individual, superando a Leónidas de Rodas, que había acumulado doce en el 152 antes de Cristo. Además, en sus últimos Juegos, Phelps fue el que más medallas ganó (6) y se convirtió en el único atleta de la historia en acumular 28 medallas olímpicas, 23 de ellas de oroEn los Juegos Olímpicos de Río 2016 se superaron varios récords tanto olímpicos como mundiales. Ocurrió en atletismo, natación, tiro, pentatlón moderno, canotaje de velocidad, levantamiento de pesas, ciclismo de pista, tiro con arco y remo. Además, anunciaron su retiro Usain Bolt y Michael Phelps, quienes volvieron a dejar sus marcas.

</p>
<br><br>
</div>
    <br>    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

<div class="col-sm-3">
    <h3 class="text-success">Record:</h3>

    <ul>
      <li><a href="RecordFo.html">Igresar nuevo record</a></li>
      <li><a href="">ingresar medallas</a></li>
      <li><a href="">Eliminar record</a></li>
      <li><a href="">Ver listado de record</a></li>
   
    </ul>



</div>

</div>

    </div> <!-- /container -->
    


<!-- PIE DE PAGINA -->
<footer class="container-fluid" style="background: #eee; padding-top: 50px; box-shadow: 0 0 20px #aaa;">
	<div class="container">
		<div class="row">
				<div class="col-sm-4 text-center">
				<a href="">Enlace número 1</a><br>
				<a href="">Enlace número 2</a><br>
				<a href="">Enlace número 3</a>
				</div>

				<div class="col-sm-4 text-center">
				<a href="">Enlace número 1</a><br>
				<a href="">Enlace número 2</a><br>
				<a href="">Enlace número 3</a>
				</div>

				<div class="col-sm-4 text-center">
				<a href="">Enlace número 1</a><br>
				<a href="">Enlace número 2</a><br>
				<a href="">Enlace número 3</a>
				</div>

				
<br><br><br><br>

		</div>
		<br><br>
		<p class="text-center"><b>&copy; 2016 Proyecto Integador 511. Todos los derechos reservados.</b><br>
		Prohibida la reproducción total o parcial.</p>
		<br><br>
		<img src="img/fondo-rio-2016-pie.png" width="100%">
	</div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>

</body>
</html>
